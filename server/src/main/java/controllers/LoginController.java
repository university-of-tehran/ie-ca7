package controllers;

import models.Student;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.ResponseEntity;
import server.Server;
import org.json.JSONException;
import org.json.JSONObject;
import services.StudentsService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.stream.Collectors;

@RestController
public class LoginController {
    @CrossOrigin(origins = "*")
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public ResponseEntity login (HttpServletRequest req) {
        try {
            String requestData = req.getReader().lines().collect(Collectors.joining());
            JSONObject body = new JSONObject(requestData);
            String studentId = body.getString("std_id");
            String password = body.getString("password");

            String jwt = StudentsService.loginIfExists(studentId, password);

            if (jwt == null || jwt.length() == 0) {
                return new ResponseEntity<>("Invalid credentials!", HttpStatus.FORBIDDEN);
            } else {
                // TODO: send jwt
                return ResponseEntity.ok("Logged in successfully!");
            }
        } catch (JSONException | IOException e) {
            System.out.println(e.toString());
            return new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
