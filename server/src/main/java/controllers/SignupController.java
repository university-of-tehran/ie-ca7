package controllers;

import models.Student;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import services.StudentsService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;
import java.util.stream.Collectors;

@RestController
public class SignupController {
    @CrossOrigin(origins = "*")
    @RequestMapping(value="/signup", method = RequestMethod.POST)
    public ResponseEntity signup (HttpServletRequest req) {
        try {
            String requestData = req.getReader().lines().collect(Collectors.joining());
            JSONObject body = new JSONObject(requestData);
            String studentId = body.getString("std_id");
            String name = body.getString("name");
            String second_name = body.getString("second_name");
            String email = body.getString("email");
            String password = body.getString("password");
            Student student = new Student(
                    studentId,name,second_name,
                    "","","","",
                    "","", email, password
            );
            StudentsService.signup(student);
            return ResponseEntity.ok("Signed up successfully!");
        } catch (JSONException | IOException e) {
            System.out.println(e.toString());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (SQLException e) {
            System.out.println(e.toString());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
