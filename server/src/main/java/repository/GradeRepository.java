package repository;

import models.Grade;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


public class GradeRepository extends Repository<Grade, String> {
    private static final String TABLE_NAME = "grades";
    private static GradeRepository instance;

    public static GradeRepository getInstance() {
        if (instance == null) {
            try {
                instance = new GradeRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in GradeRepository.create query.");
                System.out.println(e.toString());
            }
        }
        return instance;
    }

    private GradeRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format(
                        "CREATE TABLE IF NOT EXISTS %s(" +
                                "id int NOT NULL AUTO_INCREMENT,\n" +
                                "code CHAR(50),\n" +
                                "grade INT,\n" +
                                "term INT,\n" +
                                "studentId CHAR(50),\n" +
                                "PRIMARY KEY(id)" +
                                ");",
                        TABLE_NAME
                )
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT * FROM %s WHERE id = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s (id, code, grade, term, studentId) VALUES(?,?,?,?,?);", TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Grade data) throws SQLException {
        st.setInt(1, data.id);
        st.setString(2, data.code);
        st.setInt(3, data.grade);
        st.setInt(4, data.term);
        st.setString(5, data.studentId);
    }

    @Override
    protected String getFindAllStatement() {
        return String.format("SELECT * FROM %s;", TABLE_NAME);
    }

    @Override
    protected Grade convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new Grade(
            rs.getInt(1),
            rs.getString(2),
            rs.getInt(3),
            rs.getInt(4),
            rs.getString(5)
        );
    }

    @Override
    protected ArrayList<Grade> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Grade> grades = new ArrayList<>();
        rs.previous();
        while (rs.next()) {
            grades.add(this.convertResultSetToDomainModel(rs));
        }
        return grades;
    }

    public List<Grade> findByStudentId(String studentId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getFindByValueStatement());
        fillFindByValues(st, studentId);
        try {
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()) {
                st.close();
                con.close();
                return null;
            }
            List<Grade> result = convertResultSetToDomainModelList(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.find query.");
            e.printStackTrace();
            throw e;
        }
    }

    protected String getFindByValueStatement() {
        return String.format("SELECT * FROM %s WHERE studentId = ?;", TABLE_NAME);
    }

    protected void fillFindByValues(PreparedStatement st, String studentId) throws SQLException {
        st.setString(1, studentId);
    }
}
