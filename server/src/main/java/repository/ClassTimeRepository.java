package repository;

import models.ClassTime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class ClassTimeRepository extends Repository<ClassTime, Integer> {
    private static final String TABLE_NAME = "class_time";
    private static ClassTimeRepository instance;

    public static ClassTimeRepository getInstance() {
        if (instance == null) {
            try {
                instance = new ClassTimeRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in ClassTimeRepository.create query.");
                System.out.println(e.toString());
            }
        }
        return instance;
    }

    private ClassTimeRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format(
                        "CREATE TABLE IF NOT EXISTS %s(" +
                                "id int NOT NULL AUTO_INCREMENT,\n" +
                                "days CHAR(255),\n" +  // "Saturday,Sunday,Monday"
                                "time CHAR(50),\n" +
                                "UNIQUE KEY day_time_unique (days,time),\n" +
                                "PRIMARY KEY(id)" +
                                ");",
                        TABLE_NAME
                )
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT * FROM %s WHERE id = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, Integer id) throws SQLException {
        st.setInt(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s (id, days, time) VALUES(?,?,?);", TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, ClassTime data) throws SQLException {
        st.setInt(1, data.id);
        st.setString(2, String.join(",", data.days));
        st.setString(3, data.time);
    }

    @Override
    protected String getFindAllStatement() {
        return String.format("SELECT * FROM %s;", TABLE_NAME);
    }

    @Override
    protected ClassTime convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new ClassTime(
            rs.getInt(1),
            rs.getString(2),
            rs.getString(3)
        );
    }

    @Override
    protected ArrayList<ClassTime> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<ClassTime> classTimes = new ArrayList<>();
        rs.previous();
        while (rs.next()) {
            classTimes.add(this.convertResultSetToDomainModel(rs));
        }
        return classTimes;
    }

    public ClassTime findByValues(String days, String time) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = con.prepareStatement(getFindByValueStatement());
        fillFindByValues(st, days, time);
        try {
            ResultSet resultSet = st.executeQuery();
            if (!resultSet.next()) {
                st.close();
                con.close();
                return null;
            }
            ClassTime result = convertResultSetToDomainModel(resultSet);
            st.close();
            con.close();
            return result;
        } catch (Exception e) {
            st.close();
            con.close();
            System.out.println("error in Repository.find query.");
            e.printStackTrace();
            throw e;
        }
    }

    protected String getFindByValueStatement() {
        return String.format("SELECT * FROM %s WHERE days = ? AND time = ?;", TABLE_NAME);
    }

    protected void fillFindByValues(PreparedStatement st, String days, String time) throws SQLException {
        st.setString(1, days);
        st.setString(2, time);
    }
}
