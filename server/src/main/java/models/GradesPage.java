package models;

import java.util.List;

public class GradesPage {
    public Student student;
    public List<Grade> grades;
    public List<Course> courses;
}
