package models;


import java.security.MessageDigest;

public class Student {
    public String id;
    public String name;
    public String secondName;
    public String birthDate;
    public String field;
    public String faculty;
    public String level;
    public String status;
    public String img;
    public String email;
    public String password;

    public Student(
            String id, String name, String secondName,
            String birthDate, String field, String faculty,
            String level, String status, String img,
            String email, String password
    ) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.birthDate = birthDate;
        this.field = field;
        this.faculty = faculty;
        this.level = level;
        this.email = email;

        this.password = password;

        if (status == null || status.length() == 0) {
            this.status = "مشغول به تحصیل";
        } else {
            this.status = status;
        }

        if (img == null || img.length() == 0) {
            this.img = "https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png";
        } else {
            this.img = img;
        }
    }

    public String passwordToHash(String password) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(password.getBytes());
            return new String(messageDigest.digest());
        } catch (Exception e) {
            System.out.println(e.toString());
            return "not-set-due-to-hash-problem";
        }
    }

    public String getJWT() {
        // TODO: generate JWT
        return "JWT";
    }
}
