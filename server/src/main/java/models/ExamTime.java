package models;


public class ExamTime {
    public int id;
    public String start;
    public String end;

    public ExamTime(
            Integer id,
            String start,
            String end
    ) {
        this.id = id;
        this.start = start;
        this.end = end;
    }
}
