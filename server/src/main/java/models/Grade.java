package models;

public class Grade {
    public int id;
    public String code;
    public int grade;
    public int term;
    public String studentId;

    public Grade(
            Integer id,
            String code,
            Integer grade,
            Integer term,
            String studentId
    ) {
        this.id = id;
        this.code = code;
        this.grade = grade;
        this.term = term;
        this.studentId = studentId;
    }
}
