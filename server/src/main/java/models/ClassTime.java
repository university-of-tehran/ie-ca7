package models;

import java.util.Arrays;
import java.util.List;

public class ClassTime {
    public int id;
    public List<String> days;
    public String time;

    public ClassTime(
            Integer id,
            String days,
            String time
    ) {
        this.id = id;
        this.days = Arrays.asList(days.split("\\s*,\\s*"));;
        this.time = time;
    }
}
