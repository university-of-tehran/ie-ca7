package services;

import data.DataHandler;
import models.Course;
import models.Grade;
import models.Schedule;
import server.Server;

import java.util.ArrayList;
import java.util.List;

public class CoursesService {
    public static List<Course> getCourses() {
        return DataHandler.getAllCourses();
    }

    public static List<Course> loadSubmittedCourses(String studentId) {
        List<Schedule> studentSchedule = DataHandler.getPlanScheduleForStudent(studentId);
        List<Course> submittedCourses = new ArrayList<Course>();

        if (studentSchedule == null)
            return submittedCourses;

        for (Schedule s: studentSchedule) {
            Course c = DataHandler.getCourse(s.code);
            submittedCourses.add(c);
        }

        return submittedCourses;
    }

    public static boolean canBeAddedToSelectedCourses(Course newCourse) {
        List<Course> selectedCourses = Server.server.getSelectedCourses();
        for (Course course: selectedCourses) {
            if (CoursesService.classesHaveExamTimeCollision(course, newCourse) ||
                    CoursesService.classesHaveTimeCollision(course, newCourse)) {
                return false;
            }
        }
        return true;
    }

    public static boolean courseHaveCapacity(Course newCourse) {
        List<Course> allCourses = DataHandler.getAllCourses();

        for (Course course: allCourses) {
            if (course.code.equals(newCourse.code) && course.capacity > 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean classesHaveTimeCollision(Course c1, Course c2) {
        boolean day_collision = false;

        for (String d1: c1.classTime.days) {
            for (String d2: c2.classTime.days) {
                if (d1.equals(d2)) {
                    day_collision = true;
                    break;
                }
            }
        }
        if (day_collision) {
            String[] t1 = (c1.classTime.time).split("-");
            String[] t2 = (c2.classTime.time).split("-");
            String s1 = t1[0];
            String e1 = t1[1];
            String s2 = t2[0];
            String e2 = t2[1];
            return (s1.compareTo(s2) >= 0 && s1.compareTo(e2) < 0) && (e1.compareTo(s2) > 0 || e1.compareTo(e2) <= 0);
        }
        return false;
    }

    public static boolean classesHaveExamTimeCollision(Course c1, Course c2) {
        String examTimeStart1 = c1.examTime.start;
        String examTimeEnd1 = c1.examTime.end;
        String examTimeStart2 = c2.examTime.start;
        String examTimeEnd2 = c2.examTime.end;
        return (examTimeStart1.compareTo(examTimeStart2) >= 0 && examTimeStart1.compareTo(examTimeEnd2) < 0) ||
                (examTimeEnd1.compareTo(examTimeStart2) > 0 && examTimeEnd1.compareTo(examTimeEnd2) <= 0);
    }

    public static String submit() {
        List<Course> selectedCourses = Server.server.getSelectedCourses();

        int totalUnits = 0;
        for (Course c : selectedCourses) {
            if (DataHandler.getCourseSignedUpCount(c.code, c.classCode) >= c.capacity) {
                return "Course capacity is full!";
            }
            totalUnits += c.units;
        }

        if (totalUnits > 20 || totalUnits < 12) {
            return "Total unit count is not allowed!";
        }

        List<Grade> grades = DataHandler.getStudentGrades(Server.server.getLoggedInStudent().id);
        for (Course c : selectedCourses) {
            for (Grade g : grades) {
                if (g.code.equals(c.code) && g.grade >= 10) {
                    return "You already passed " + c.name;
                }
            }
        }

        for (Course c : selectedCourses) {
            boolean seenFlag = false;
            if (c.prerequisites == null || c.prerequisites.size() == 0 || c.prerequisites.get(0).equals(""))
                continue;
            
            for (String prerequisite : c.prerequisites) {
                seenFlag = false;
                for (Grade grade : grades) {
                    if (grade.code.equals(prerequisite)) {
                        seenFlag = true;
                        if (grade.grade >= 10) {
                            break;
                        } else {
                            return "Prerequisites not satisfied " + c.name;
                        }
                    }
                }
                if (!seenFlag) {
                    return "Prerequisites not satisfied " + c.name;
                }
            }
        }

        List<Schedule> schedules = DataHandler.getAllSchedules();
        schedules.removeIf(s -> s.studentId.equals(Server.server.getLoggedInStudent().id));

        List<Schedule> newSchedules = new ArrayList<>();
        for (Course c : selectedCourses) {
            Schedule newSchedule = new Schedule(null,null,null,null, null);
            newSchedule.studentId = Server.server.getLoggedInStudent().id;
            newSchedule.code = c.code;
            newSchedule.classCode = c.classCode;
            newSchedule.status = "submitted";
            newSchedules.add(newSchedule);
        };
        DataHandler.writeSchedulesToDB(newSchedules);
        Server.server.resetLastSubmit();
        return "";
    }
}
