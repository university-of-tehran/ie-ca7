package services;

import data.DataHandler;
import models.Student;
import server.Server;

import java.sql.SQLException;
import java.util.List;

public class StudentsService {
    public static String loginIfExists(String studentId, String password) {
        Student student = DataHandler.getStudent(studentId);

        if (student == null) {
            return null;
        } else if (!student.password.equals(student.passwordToHash(password))) {
            return null;
        } else {
            Server.server.logInStudent(student);
            return student.getJWT();
        }
    }

    public static void logout() {
        Server.server.logOutStudent();
    }

    public static void signup(Student student) throws SQLException {
        Server.server.signUpStudent(student);
    }
}
