import React, { useState, useContext } from "react";
import { Button } from "react-bootstrap";
import axios from "axios";
import { useHistory } from "react-router-dom";
import ClipLoader from "react-spinners/ClipLoader";

import SignInModal from "../SignInModal";
import SignOutModal from "../SignOutModal";
import RegisterModal from "../RegisterModal";
import LoadingContext from "../../contexts/LoadingContext";

import "./style.css";
import logo from "../../assets/logo.png";

const spinnerStyle = {
    display: "block",
    margin: "100px auto",
    borderColor: "#1FA5FF",
};

export default function Header() {
    const history = useHistory();

    const { setLoading, loading } = useContext(LoadingContext);

    const [signedIn, setSignedIn] = useState(false);
    axios.get("http://localhost:8080/profile").then(
        (response) => {
            setSignedIn(true);
        },
        (error) => {}
    );

    const [signIn, setSignIn] = useState(false);
    const [register, setRegister] = useState(false);
    const [signOut, setSignOut] = useState(false);

    const toggleSignIn = () => setSignIn(!signIn);
    const toggleSignOut = () => setSignOut(!signOut);
    const toggleRegister = () => setRegister(!register);

    const handleSignIn = (stdId, setError) => {
        setLoading(true);
        axios
            .post("http://localhost:8080/login", {
                std_id: stdId,
            })
            .then(
                (response) => {
                    setSignIn(false);
                    setSignedIn(true);
                    history.go(0);
                    setLoading(false);
                },
                (error) => {
                    if (error.response) {
                        setError(error.response.data);
                    } else {
                        setError(error);
                    }
                    setLoading(false);
                }
            );
    };

    const handleSignOut = () => {
        axios.get("http://localhost:8080/logout").then(
            (response) => {
                console.log(response);
                setSignOut(false);
                setSignedIn(false);
                history.push("/");
                history.go(0);
            },
            (error) => {
                console.log(error);
            }
        );
    };

    return (
        <>
            <ClipLoader loading={loading} css={spinnerStyle} size={150} />
            <div class="site-header w-100">
                <div class="header-right-side">
                    <a class="header-menu-item" href="../">
                        <img class="header-logo" src={logo} alt="logo" />
                    </a>
                    <div class="header-menu">
                        <a class="header-menu-item" href="../courses">
                            انتخاب واحد
                        </a>
                        <a class="header-menu-item" href="../schedule">
                            برنامه هفتگی
                        </a>
                    </div>
                </div>
                {signedIn ? (
                    <div class="header-left-side">
                        <Button variant="danger" onClick={toggleSignOut}>
                            خروج
                        </Button>
                    </div>
                ) : (
                    <div class="flex">
                        <Button variant="primary mr-2" onClick={toggleRegister}>
                            ثبت نام
                        </Button>
                        <Button
                            variant="outline-secondary"
                            onClick={toggleSignIn}
                        >
                            ورود
                        </Button>
                    </div>
                )}
            </div>

            <SignInModal
                show={signIn}
                handleClose={toggleSignIn}
                handleSignIn={handleSignIn}
            />
            <SignOutModal
                show={signOut}
                handleClose={toggleSignOut}
                handleSignOut={handleSignOut}
            />
            <RegisterModal show={register} handleClose={toggleRegister} />
        </>
    );
}
